import { Menu } from "antd";
import "../Navbar/navbar.css"

const Navbar = () => {
  return (
    <Menu mode="horizontal">
      <h2 className="headname">Employee Data</h2>
    </Menu>
  );
};

export default Navbar;
