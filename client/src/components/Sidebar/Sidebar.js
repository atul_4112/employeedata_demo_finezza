import React from "react";
import { Layout, Menu } from "antd";
import "../Sidebar/sidebar.css"
import EmployeeDetails from "../Employee/EmployeeDetailes";
import AddEmployee from "../Employee/AddEmployee";
// import { Route, Routes } from "react-router-dom";
// import UpdateEmployee from "../Employee/UpdateEmployee";

const { Sider, Content } = Layout;

class Sidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedMenuItem: null,
    };
  }

  handleMenuClick = (e) => {
    this.setState({ selectedMenuItem: e.key });
  };
  componentDidMount() {
    this.setState({ selectedMenuItem: "employee" });
  }
  render() {
    return (
      <Layout>
        <Sider className="sidermain">
          <Menu
            onClick={this.handleMenuClick}
            mode="inline"
            defaultSelectedKeys={["employee"]}
            className="menusider"
          >
            <Menu.Item key="employee">Employee Details</Menu.Item>
            <Menu.Item key="addemployee">Add new employee</Menu.Item>

            {/* <Menu.Item key="news">News Update</Menu.Item>
            <Menu.Item key="holiday">Holiday</Menu.Item> */}
          </Menu>
        </Sider>
        <Content>
          {this.state.selectedMenuItem === "employee" && <EmployeeDetails />}
          {this.state.selectedMenuItem === "addemployee" && <AddEmployee />}
          {/* {this.state.selectedMenuItem === "holiday" && <HolidayList />} */}
        </Content>
        {/* <Content>
          {this.state.selectedMenuItem === "employee" && (
            <Routes>
              <Route path="/" element={<EmployeeDetails />} />
            </Routes>
          )}
          {this.state.selectedMenuItem === "addemployee" && <AddEmployee />}
          <Routes>
            <Route path="/emp/:id" element={<UpdateEmployee />} />
          </Routes>
        </Content> */}
      </Layout>
    );
  }
}

export default Sidebar;
