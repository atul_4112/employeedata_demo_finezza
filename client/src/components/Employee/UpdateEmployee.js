import { Component } from "react";
import {
  Form,
  Input,
  DatePicker,
  Radio,
  Button,
  Card,
  Modal,
  message,
  Alert,
  Image,
  Popconfirm,
} from "antd";
import "./employee.css";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import {
  getEmployeeById,
  deleteEmployeeById,
  updateEmployeeByID,
} from "../../APIList";
import Loading from "../Loading/Loader";
import moment from "moment";

class UpdateEmployee extends Component {
  constructor() {
    super();
    this.state = {
      error: "",
      isModalOpen: false,
      getIdError: "",
      employeeData: [],
      isLoading: false,
    };
  }

  handleId = async (e) => {
    const id = this.props.id;
    try {
      const data = await getEmployeeById(id);
      this.setState({
        employeeData: data,
        isLoading: false,
      });
    } catch (error) {
      this.setState({
        getIdError: error.response.data.message,
      });
    }
  };

  showModal = () => {
    this.setState({
      isModalOpen: true,
    });
  };

  handleCancel = () => {
    this.setState({
      isModalOpen: false,
    });
  };

  onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  onFinish = async (values) => {
    const id = this.props.id;
    try {
      await updateEmployeeByID(id, values);
      message.success("Newsletter updated successfully!");
      this.setState({
        isModalOpen: false,
      });
      setTimeout(() => {
        window.location.href = "/hr";
      }, "1000");
      this.formRef.current.resetFields();
    } catch (error) {
      this.setState({
        getIdError: error.response.data.message,
      });
    }
  };

  handleDelete = async () => {
    try {
      const id = this.props.id;
      const data = await deleteEmployeeById(id);
      this.setState({
        employeeData: data,
        isLoading: false,
        isModalOpen: false,
      });
      message.success("Newsletter deleted successfully!");
      setTimeout(() => {
        window.location.href = "/";
      }, "1000");
    } catch (error) {
      this.setState({
        getIdError: error.response.data.message,
      });
    }
  };

  render() {
    const { employeeData, isLoading, getIdError } = this.state;
    return (
      <>
        <Button
          icon={<EditOutlined />}
          type="primary"
          onClick={(e) => {
            this.showModal();
            this.handleId();
          }}
        >
          Edit
        </Button>
        <div>
          <div className="employeetable">
            <Modal
              title="Update employee"
              open={this.state.isModalOpen}
              footer={null}
              onCancel={this.handleCancel}
            >
              <div>
                <Form
                  ref={this.formRef}
                  name="employee_update"
                  onFinish={this.onFinish}
                  layout="vertical"
                  initialValues={{
                    remember: true,
                  }}
                >
                  <Card>
                    {isLoading && <Loading />}
                    {getIdError ? (
                      <Alert
                        description={getIdError}
                        type="error"
                        showIcon
                        style={{ marginBottom: 16 }}
                      />
                    ) : (
                      employeeData.map((data) => (
                        <>
                          <div>
                            <div>
                              <Form.Item label="Name" name="emp_name">
                                <Input defaultValue={data.emp_name} />
                              </Form.Item>
                            </div>
                            <Image
                              style={{
                                width: "200px",
                                height: "250px",
                                objectFit: "cover",
                              }}
                              src={data.emp_profileUrl}
                            />
                          </div>
                          <Form.Item
                            label="Profile Picture"
                            name="emp_profileUrl"
                          >
                            <Input defaultValue={data.emp_profileUrl} />
                          </Form.Item>

                          <Form.Item label="Email" name="emp_email">
                            <Input type="email" defaultValue={data.emp_email} />
                          </Form.Item>

                          <Form.Item label="Contact Number" name="emp_contact">
                            <Input type="tel" defaultValue={data.emp_contact} />
                          </Form.Item>

                          <Form.Item label="Address" name="emp_address">
                            <Input.TextArea defaultValue={data.emp_address} />
                          </Form.Item>

                          <Form.Item label="Pincode" name="emp_pincode">
                            <Input
                              type="number"
                              defaultValue={data.emp_pincode}
                            />
                          </Form.Item>

                          <Form.Item label="Date of Birth" name="emp_dob">
                            <DatePicker defaultValue={moment(data.emp_dob)} />
                          </Form.Item>

                          <Form.Item label="Gender" name="emp_gender">
                            <Radio.Group defaultValue={data.emp_gender}>
                              <Radio value="male">Male</Radio>
                              <Radio value="female">Female</Radio>
                              <Radio value="other">Other</Radio>
                            </Radio.Group>
                          </Form.Item>

                          <Form.Item
                            label="Marital Status"
                            name="emp_martialStatus"
                          >
                            <Radio.Group defaultValue={data.emp_martialStatus}>
                              <Radio value="single">Single</Radio>
                              <Radio value="married">Married</Radio>
                              <Radio value="divorced">Divorced</Radio>
                            </Radio.Group>
                          </Form.Item>
                        </>
                      ))
                    )}
                    <Form.Item>
                      <div className="buttoncrud">
                        <Button
                          type="primary"
                          htmlType="submit"
                          icon={<EditOutlined />}
                        >
                          Update
                        </Button>
                        <Popconfirm
                          title="Are you sure you want to delete this News?"
                          onConfirm={this.handleDelete}
                          okText="Yes"
                          cancelText="No"
                        >
                          <Button
                            type="primary"
                            icon={<DeleteOutlined />}
                            danger
                          >
                            Delete
                          </Button>
                        </Popconfirm>
                      </div>
                    </Form.Item>
                  </Card>
                </Form>
              </div>
            </Modal>
          </div>
        </div>
      </>
    );
  }
}

export default UpdateEmployee;
