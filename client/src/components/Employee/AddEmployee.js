import React, { Component } from "react";
import { Form, Input, DatePicker, Radio, Button, Card,message,Alert } from "antd";
// import moment from 'moment'
import { addEmployee } from "../../APIList";


class AddEmployee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
    };
  }
  onFinish = async (values) => {
    // const dob = moment(values.emp_dob).format("YYYY-MM-DD");
    // console.log("Date of birth:", dob);
    try {
      await addEmployee(values);
      message.success("Employee created succesfully.");
      setTimeout(() => {
        window.location.href = "/";
      }, "1000");
    } catch (error) {
      this.setState({
        error: error.response.data.errors,
      });
    }
  };
  render() {
    const {error } = this.state
    return (
      <div className="employeetable">
        <h2>Add new employee</h2>
        <Card>
          {error && <Alert description={error} type="error" showIcon />}
          <Form
            name="employee_form"
            onFinish={this.onFinish}
            initialValues={{
              emp_gender: "male",
              emp_martialStatus: "single",
            }}
          >
            <Form.Item
              label="Name"
              name="emp_name"
              rules={[{ required: true, message: "Please input your name!" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Profile Picture"
              name="emp_profileUrl"
              rules={[
                { required: true, message: "Please input your profile url!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Email"
              name="emp_email"
              rules={[{ required: true, message: "Please input your email!" }]}
            >
              <Input type="email" />
            </Form.Item>

            <Form.Item
              label="Contact Number"
              name="emp_contact"
              rules={[
                {
                  required: true,
                  message: "Please input your contact number!",
                },
              ]}
            >
              <Input type="tel" />
            </Form.Item>

            <Form.Item
              label="Address"
              name="emp_address"
              rules={[
                { required: true, message: "Please input your address!" },
              ]}
            >
              <Input.TextArea />
            </Form.Item>

            <Form.Item
              label="Pincode"
              name="emp_pincode"
              rules={[
                { required: true, message: "Please input your pincode!" },
              ]}
            >
              <Input type="number" />
            </Form.Item>

            <Form.Item
              label="Date of Birth"
              name="emp_dob"
              rules={[
                {
                  required: true,
                  message: "Please select your date of birth!",
                },
              ]}
            >
              <DatePicker />
            </Form.Item>

            <Form.Item label="Gender" name="emp_gender">
              <Radio.Group>
                <Radio value="male">Male</Radio>
                <Radio value="female">Female</Radio>
                <Radio value="other">Other</Radio>
              </Radio.Group>
            </Form.Item>

            <Form.Item label="Marital Status" name="emp_martialStatus">
              <Radio.Group>
                <Radio value="single">Single</Radio>
                <Radio value="married">Married</Radio>
                <Radio value="divorced">Divorced</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  }
}

export default AddEmployee;
