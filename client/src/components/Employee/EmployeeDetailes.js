import { Component } from "react";
import { getAllEmployee } from "../../APIList";
import { Space, Table, Alert, Image } from "antd";
import Loading from "../Loading/Loader";
import "../Employee/employee.css";
import UpdateEmployee from "./UpdateEmployee";
// import { NavLink } from "react-router-dom";

class EmployeeDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      empData: [],
      isLoading: false,
      error: "",
      isModalOpen: false,
    };
  }

  getAllEmployee = async () => {
    this.setState({
      isLoading: true,
    });
    try {
      const response = await getAllEmployee();
      this.setState({
        empData: response,
        isLoading: false,
      });
    } catch (error) {
      this.setState({
        error: "Failed while fetching data..!" + error.message,
      });
    }
  };
  componentDidMount() {
    this.getAllEmployee();
  }

  renderAction = (data) => {
    return (
      <Space>
        <UpdateEmployee id={data.id}> </UpdateEmployee>
      </Space>
    );
  };

  columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Name",
      dataIndex: "emp_name",
      key: "emp_name",
    },
    {
      title: "Email",
      dataIndex: "emp_email",
      key: "emp_email",
    },
    {
      title: "Profile",
      dataIndex: "emp_profileUrl",
      key: "emp_profileUrl",
      render: (url) => (
        <Image
          style={{ width: "80px", height: "80px", objectFit: "cover" }}
          src={url}
        />
      ),
    },
    {
      title: "Actions",
      dataIndex: "action",
      render: (text, row) => this.renderAction(row),
    },
  ];
  render() {
    const { empData, error, isLoading } = this.state;
    return (
      <div className="employeetable">
        <h2>Employe details</h2>
        {isLoading && <Loading />}
        {error && <Alert description={error} type="error" showIcon />}
        <Table size="small" dataSource={empData} columns={this.columns} />
      </div>
    );
  }
}

export default EmployeeDetails;
