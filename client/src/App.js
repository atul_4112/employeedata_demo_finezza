import "./App.css";
import Employees from "./components/Employee/Employees";
import Navbar from "./components/Navbar/Navbar";

function App() {
  return (
    <div>
      <Navbar />
      <Employees />
    </div>
  );
}

export default App;
