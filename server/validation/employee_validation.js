const Joi = require("joi");

const addEmployeeValidation = Joi.object({
  emp_title: Joi.string().valid("Mr.", "Mrs.", "Ms.", "Msrs."),
  emp_name: Joi.string()
    .required()
    .max(100)
    .custom((value, helpers) => {
      const cleanValue = value.replace(/<\/?[^>]+(>|$)/g, "");
      if (value !== cleanValue) {
        return helpers.error(
          "Text field should not contain HTML tags or special characters"
        );
      }
      return cleanValue;
    }),
  emp_email: Joi.string().email().required(),
  emp_contact: Joi.string().pattern(new RegExp("^[0-9]{10}$")).required(),
  emp_address: Joi.string().required(),
  emp_pincode: Joi.string().pattern(new RegExp("^[0-9]{6}$")).required(),
  emp_dob: Joi.date().required(),
  emp_education: Joi.string().required(),
  emp_gender: Joi.string().valid("male", "female", "other").required(),
  emp_martialStatus: Joi.string()
    .valid("single", "married", "divorced", "widowed")
    .required(),
  emp_profileUrl: Joi.string().uri(),
  emp_tag: Joi.string().required(),
});

const updateEmployeeValidation = Joi.object({
  emp_title: Joi.string().valid("Mr.", "Mrs.", "Ms.", "Msrs."),
  emp_name: Joi.string()
    .optional()
    .max(100)
    .custom((value, helpers) => {
      const cleanValue = value.replace(/<\/?[^>]+(>|$)/g, "");
      if (value !== cleanValue) {
        return helpers.error(
          "Text field should not contain HTML tags or special characters"
        );
      }
      return cleanValue;
    }),
  emp_email: Joi.string().email().optional(),
  emp_contact: Joi.string().pattern(new RegExp("^[0-9]{10}$")).optional(),
  emp_address: Joi.string().optional(),
  emp_pincode: Joi.string().pattern(new RegExp("^[0-9]{6}$")).optional(),
  emp_dob: Joi.date().optional(),
  emp_education: Joi.string().optional(),
  emp_gender: Joi.string().valid("male", "female", "other").optional(),
  emp_martialStatus: Joi.string()
    .valid("single", "married", "divorced", "widowed")
    .optional(),
  emp_profileUrl: Joi.string().uri(),
  emp_tag: Joi.array().items(Joi.string().trim().max(20)).optional(),
});

module.exports = { addEmployeeValidation, updateEmployeeValidation };
