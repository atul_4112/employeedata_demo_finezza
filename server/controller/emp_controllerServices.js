const db = require("../models");
const {
  addEmployeeValidation,
  updateEmployeeValidation,
} = require("../validation/employee_validation");


const employeeModel = db.employee

const employeeAddServices = async (info, employeeModel, res) => {
  const validationResponse = addEmployeeValidation.validate(info);

  if (validationResponse.error) {
    return res.status(400).json({
      message: "validation Failed",
      errors: validationResponse.error.details.map((err) => err.message),
    });
  }
  try {
    const empData = await employeeModel.create({
      emp_title: info.emp_title,
      emp_name: info.emp_name,
      emp_email: info.emp_email,
      emp_contact: info.emp_contact,
      emp_address: info.emp_address,
      emp_pincode: info.emp_pincode,
      emp_dob: info.emp_dob,
      emp_education:info.emp_education,
      emp_gender: info.emp_gender,
      emp_martialStatus: info.emp_martialStatus,
      emp_profileUrl: info.emp_profileUrl,
      emp_tag:info.emp_tag
    });
    res.status(200).json({
      message: "Data created..!",
      Data: empData,
    });
  } catch (error) {
    res.status(500).json({
      message: "Something went wrong..! Failed to created Data.",
      error: error,
    });
  }
};

const getAllEmployeeService = async (req, res) => {
  try {
    const employeeData = await employeeModel.findAll({});
    res.status(200).send(employeeData);
  } catch (error) {
    res.status(500).json({
      message: "Something went wrong..! Failed to fetch Data.",
      error: error,
    });
  }
};

const getEmployeeByiIdService = async (req, res) => {
  try {
    let id = req.params.id;
    const employeeData = await employeeModel.findOne({ where: { id: id } });
    if (employeeData) {
      res.status(200).send(employeeData);
    } else {
      res.status(404).json({
        message: "News letter ID doesn't exist: " + id,
      });
    }
  } catch {
    res.status(500).json({
      message: "Something went wrong..! Failed to fetch Data.",
      error: error,
    });
  }
};

const updateEmployeeByIdService = async (req, res) => {
  const validationResponse = updateEmployeeValidation.validate(req.body);
  if (validationResponse.error) {
    return res.status(400).json({
      message: "validation Failed",
      errors: validationResponse.error.details.map((err) => err.message),
    });
  }
  try {
    let id = req.params.id;
    const employeeData = await employeeModel.update(req.body, {
      where: { id: id },
    });
    if (employeeData[0] === 1) {
      const employeeData = await employeeModel.findOne({
        where: { id: id },
      });
      res.status(200).json({
        message: "Data updated succesfully..!",
        data: employeeData,
      });
    } else {
      res.status(404).json({
        message: "Something went wrong.!",
        errorMessage: error.message,
      });
    }
  } catch (error) {
    res.status(404).json({
      message: "ID doesn't exist.",
    });
  }
};

const deletEmployeeByIdService = async (req, res) => {
  try {
    let id = req.params.id;
    const result = await employeeModel.findOne({ where: { id: id } });
    if (result === null) {
      res.status(404).json({
        message: "ID not found: " + id,
      });
    } else {
      await employeeModel.destroy({ where: { id: id } });
      res.status(200).json({
        message: "ID deleted succesfully: " + id,
      });
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
};


module.exports = {
  employeeAddServices,
  getAllEmployeeService,
  getEmployeeByiIdService,
  updateEmployeeByIdService,
  deletEmployeeByIdService,
};
