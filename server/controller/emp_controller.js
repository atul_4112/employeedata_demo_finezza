const db = require("../models");
const {
  employeeAddServices,
  getAllEmployeeService,
  getEmployeeByiIdService,
  updateEmployeeByIdService,
  deletEmployeeByIdService,
} = require("../controller/emp_controllerServices");

const employeeModel = db.employee

const employeeAdd = async (req, res) => {
  let info = {
    emp_title:req.body.emp_title,
    emp_name: req.body.emp_name,
    emp_email: req.body.emp_email,
    emp_contact: req.body.emp_contact,
    emp_address: req.body.emp_address,
    emp_pincode: req.body.emp_pincode,
    emp_dob: req.body.emp_dob,
    emp_education:req.body.emp_education,
    emp_gender: req.body.emp_gender,
    emp_martialStatus: req.body.emp_martialStatus,
    emp_profileUrl:req.body.emp_profileUrl,
    emp_tag:req.body.emp_tag,
  };

  employeeAddServices(info, employeeModel, res);
};

const getAllEmployee = async (req, res) => {
  getAllEmployeeService(req, res);
};

const getEmployeeByiId = async (req, res) => {
 
  getEmployeeByiIdService(req, res);
};

const updateEmployeeById = async (req, res) => {
  updateEmployeeByIdService(req, res);
};

const deletEmployeeById = async (req, res) => {
  deletEmployeeByIdService(req, res);
};

module.exports = {
  employeeAdd,
  getAllEmployee,
  getEmployeeByiId,
  updateEmployeeById,
  deletEmployeeById,
};
