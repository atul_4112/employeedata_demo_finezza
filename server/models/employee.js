"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class employee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  employee.init(
    {
      emp_title: DataTypes.STRING, // Add this line
      emp_name: DataTypes.STRING,
      emp_email: DataTypes.STRING,
      emp_contact: DataTypes.STRING,
      emp_address: DataTypes.STRING,
      emp_pincode: DataTypes.STRING,
      emp_dob: DataTypes.STRING,
      emp_education:DataTypes.STRING, // added new
      emp_gender: DataTypes.STRING,
      emp_martialStatus: DataTypes.STRING,
      emp_profileUrl: DataTypes.STRING, // Add this line
      emp_tag:DataTypes.STRING // Added new
    },
    {
      sequelize,
      modelName: "employee",
    }
  );
  return employee;
};
