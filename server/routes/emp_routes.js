
const employeeController = require("../controller/emp_controller");

const router = require("express").Router();

router.post("/addemployee", employeeController.employeeAdd);

router.get("/getAllEmployee", employeeController.getAllEmployee);

router.get("/:id", employeeController.getEmployeeByiId);

router.put("/:id", employeeController.updateEmployeeById);

router.delete("/:id", employeeController.deletEmployeeById);

module.exports = router;
