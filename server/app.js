const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(cors(corsOptions));

var corsOptions = {
  origin: "http://localhost:3000",
};

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

const employeeRoutes = require("./routes/emp_routes");

app.use("/emp", employeeRoutes);

module.exports = app;
